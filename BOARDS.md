List of boards connected and their owner
========================================


| Owner             | Name
|-------------------|------------------------------|
| FU Berlin         | msba2                        |
| HAW               | nucleo-f103rb                |
| Inria             | arduino-mega2560             |
| Inria             | frdm-k64f                    |
| Inria             | frdm-kw41z                   |
| Inria             | mulle                        |
| Inria             | nrf52dk                      |
| Inria             | pba-d-01-kw2x                |
| Inria             | sltb001a                     |
| Inria             | stm32f3discovery             |
|                   |                              |
