#! /usr/bin/env python3

"""Control a tty connected througha ykushxs.

Allows power cycling with detection of tty presence.
"""

import logging
import os.path
import time
import subprocess
import argparse


def wait_cond(timeout, value, fct, *args, **kwargs):
    """ Wait at max `timeout` for `fct(*args, **kwargs)` to return `value`
    :return: True if fct has returned `value` before timeout False otherwise.
    :rtype: bool
    """
    time_ref = time.time()
    while True:
        if value == fct(*args, **kwargs):
            return True
        if time.time() > (time_ref + timeout):
            break
        time.sleep(0.1)
    return False


class YkushXSTTY:
    """Control of a 'tty' connected through a YkushXS

    :param tty: powerd controled tty file descriptor

    """
    OPS_DELAY = 1
    TTY_WAIT_DEFAULT = 5

    def __init__(self, tty, serial=None, delay=1, ttywait=5):
        self.tty = tty

        self.ykush_cmd = ['ykushcmd', 'ykushxs']
        self.ykush_cmd += ['-s', serial] if serial is not None else []

        self.delay = delay
        self.ttywait = ttywait

    def power_set(self, powered):
        """Set ykushxs power to 'powered'.

        It checks for the tty presence and wait delays after operations.

        :type powered: boolean
        """
        powermode = ['-u'] if powered else ['-d']
        cmd = self.ykush_cmd + powermode

        logging.info(cmd)
        subprocess.check_call(cmd)
        ret = wait_cond(self.ttywait, powered, os.path.exists, self.tty)

        state = 'present' if os.path.exists(self.tty) else 'absent'

        if ret:
            logging.info('TTY %s in state %s', self.tty, state)
        else:
            logging.warning('TTY %s in invalid state %s', self.tty, state)

        time.sleep(self.delay)
        return ret

    def power_reset(self):
        """Power reset the ykushxs connected tty."""
        self.power_set(False)
        return self.power_set(True)


PARSER = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
PARSER.add_argument('--verbose', action='store_true')
PARSER.add_argument('--retries', type=int, default=5)
PARSER.add_argument('--ykushserial', help='ykushxs serial')
PARSER.add_argument('--delay', type=float, default=YkushXSTTY.OPS_DELAY,
                    help='Delay between operations')
PARSER.add_argument('--ttywait', type=float,
                    default=YkushXSTTY.TTY_WAIT_DEFAULT,
                    help='Delay until tty should change')

PARSER.add_argument('tty')


def main():
    """Try powering on and off the ykushxs until the board comes back."""
    args = PARSER.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.INFO)

    ykush = YkushXSTTY(args.tty, serial=args.ykushserial,
                       ttywait=args.ttywait, delay=args.delay)

    for _ in range(args.retries):
        if ykush.power_reset():
            return 0

    return 1


if __name__ == '__main__':
    main()
