Using with 'vaduz' boards
=========================


Configure your `.ssh/config` to allow doing `ssh vaduz.imp.fu-berlin.de`
directly. So no password asking, no other options required.

When accessing from outside of FU, you need to go through the ssh frontend

    Host vaduz.imp.fu-berlin.de
      ProxyCommand ssh login.imp.fu-berlin.de -W %h:%p


Then in your environment do

    export RIOT_MAKEFILES_GLOBAL_PRE=${THIS_DIR}/vaduz-boards.mk.pre


To reserve the boards for your usage run a job for your username.
Note, if you still have a terminal open after the job ends, nothing will stop
it. It is only a helper for not bothering other colleagues so please use with
dilligence and be sure to stop usage when the job is not running.

    https://ci-ilab.imp.fu-berlin.de/job/reserve-boards/


Then to use the boards on `vaduz` simply do:

    # List all the boards there (some may not be connected)
    VADUZ=1 make --no-print-directory -C examples/hello-world list-boards

    BOARD=frdm-k64f VADUZ=1 make --no-print-directory -C test/shell term
    BOARD=frdm-k64f VADUZ=1 make --no-print-directory -C test/shell reset
    # The board should support FLASHFILE to be flashed
    BOARD=frdm-k64f VADUZ=1 make --no-print-directory -C test/shell flash
