riot-buildsystem-ci-server
==========================

Repository to setup the test server for RIOT buildsystem development

This also based on a default configuration provided by the IT for a docker
ready image.

The installation is minimal and is planed to execute as many tests as possible
using the RIOT docker integration.

[List of connected boards](BOARDS.md)
